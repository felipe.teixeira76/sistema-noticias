<?php


///seção Home
Route::get('/', function () {
    return view('home.index');
});


//seção Noticia
Route::get('/tecnologia', function () {
    return view('noticias.index');
});

Route::get('/tecnologia/titulo-noticia', function () {
    return view('noticias.visualizar');
});

Route::get('/contato', function () {
    return view('home.contato');
});


///seção Admin
Route::get('/admin/home', function () {
    return view('admin.home.index');
});


//seção admin/noticia
Route::get('/admin/noticias','Admin\NoticiasController@index');
    


Route::get('/admin/noticias/cadastrar', 'Admin\NoticiasController@Cadastrar');
   


Route::get('/admin/noticias/editar', 'Admin\NoticiasController@Editar');
    


Route::get('/admin/noticias/visualizar', 'Admin\NoticiasController@Visualizar');

Route::get('/admin/noticias/deletar', 'Admin\NoticiasController@Deletar');


    


//seção admin/categoria
Route::get('/admin/categorias', 'Admin\CategoriasController@index');

Route::get('/admin/categorias/visualizar', 'Admin\CategoriasController@visualizar');

Route::get('/admin/categorias/editar', 'Admin\CategoriasController@editar');

Route::get('/admin/categorias/cadastrar', 'Admin\CategoriasController@cadastrar');

Route::get('/admin/categorias/deletar', 'Admin\CategoriasController@deletar');

    



//seção admin/usuario
Route::get('/admin/usuarios', 'Admin\usuariosController@index');

Route::get('/admin/usuarios/cadastrar', 'Admin\usuariosController@cadastrar');

Route::get('/admin/usuarios/editar', 'Admin\usuariosController@editar');

Route::get('/admin/usuarios/visualizar', 'Admin\usuariosController@visualizar');
    
