@extends('layouts.admin')

@section('titulo','area administrativa')

@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>
                Usuario
            </h2>


        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Permissão</th>
                        <th>Status</th>
                        <th>Data Cadastro</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>



                
                <tr>
                    <td scope="row">1</td>
                    <td>
                        <div class="row">
                            <div class="col-12">
                                @include('admin.usuarios.form')
                            </div>

                        </div>
                    </td>
                    <td> 
                        <input class="form-control" type="text">
                    </td>
                    <td>
                            <select name="status" id="status" class="form-control">
                                    <option value="0">Selecione</option>
                                    <option value="1">Usuario</option>
                                    <option value="2">Administrador</option>
                                </select>
                    </td>
                    <td>
                        <input class="form-control" type="number">
                    </td>

                    <td>
                        20/05/2019
                    </td>
                    <td>
                        ...
                    </td>
                    
        </div>



        </tr>

        
        </tr>
        </table>
        </tbody>


    </div>


</div>

</div>

@endsection
