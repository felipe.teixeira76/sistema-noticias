@extends('layouts.app')

@section('titulo', 'Tecnologia')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Tecnologia</h2>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 mx-auto">
                <article class="box-noticia">
                    <h2>Titulo Noticia</h2>
                    <p>06/05/2019</p>
                    <p class="text-center p-5">
                        <img class="img-fluid" src="https://via.placeholder.com/800x400">
                    </p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, deleniti earum! Corrupti modi minima hic distinctio inventore eos eius suscipit temporibus dolor repellat saepe odio obcaecati voluptas, incidunt ipsum ea.</p>
                </article>
            </div>        
        </div>
    </div>
@endsection
